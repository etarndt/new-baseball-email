import pymongo
import config

SEED_DATA = [
    {
        "station": "WACD",
        "city": "Antigo",
                "channels": "106.1",
                "lat": 45.106357,
                "long": -89.152613
    },
    {
        "station": "WHBY",
        "city": "Appleton",
                "channels": "1150/103.5/106.3",
                "lat": 44.138874,
                "long": -88.546222
    },
    {
        "station": "WBEV",
        "city": "Beaver Dam",
                "channels": "1430",
                "lat": 43.428602,
                "long": -88.892608
    },
    {
        "station": "WISS",
        "city": "Berlin",
                "channels": "1100/98.7",
                "lat": 43.948592,
                "long": -88.985948
    },
    {
        "station": "WWIS",
        "city": "Black River Falls",
                "channels": "99.7",
                "lat": 44.319683,
                "long": -90.892088
    },
    {
        "station": "WATQ",
        "city": "Eau Claire",
                "channels": "106.7",
                "lat": 45.184405,
                "long": -91.731282
    },
    {
        "station": "KFIZ",
        "city": "Fond du Lac",
                "channels": "1450",
                "lat": 43.7911,
                "long": -88.471219
    },
    {
        "station": "WFAW",
        "city": "Fort Atkinson",
                "channels": "940",
                "lat": 42.906674,
                "long": -88.751772
    },
    {
        "station": "WTAQ",
        "city": "Green Bay",
                "channels": "1360/97.5",
                "lat": 44.405825,
                "long": -88.00538
    },
    {
        "station": "WRLS",
        "city": "Hayward",
                "channels": "92.3",
                "lat": 46.020506,
                "long": -91.511566
    },
    {
        "station": "WNXR",
        "city": "Iron River",
                "channels": "107.3",
                "lat": 46.546886,
                "long": -91.414079
    },
    {
        "station": "WCLO",
        "city": "Janesville",
                "channels": "1230/92.7",
                "lat": 42.659733,
                "long": -89.042333
    },
    {
        "station": "WKTY",
        "city": "La Crosse",
                "channels": "580/96.7",
                "lat": 43.740246,
                "long": -91.205967
    },
    {
        "station": "WIBA",
        "city": "Madison",
                "channels": "1310",
                "lat": 42.999442,
                "long": -89.429842
    },
    {
        "station": "WOMT",
        "city": "Manitowoc",
                "channels": "1240",
                "lat": 44.125274,
                "long": -87.628139
    },
    {
        "station": "WMAM",
        "city": "Marinette",
                "channels": "570",
                "lat": 45.100541,
                "long": -87.625107
    },
    {
        "station": "WIGM",
        "city": "Medford",
                "channels": "1490",
                "lat": 45.164136,
                "long": -90.341252
    },
    {
        "station": "WTMJ",
        "city": "Milwaukee",
                "channels": "620/103.3",
                "lat": 42.707796,
                "long": -88.065918
    },
    {
        "station": "WCQM",
        "city": "Park Falls",
                "channels": "98.3",
                "lat": 45.882177,
                "long": -90.437661
    },
    {
        "station": "WPVL",
        "city": "Platteville",
                "channels": "1590",
                "lat": 42.755548,
                "long": -90.505685
    },
    {
        "station": "WPDR",
        "city": "Portage",
                "channels": "1350",
                "lat": 43.528315,
                "long": -89.433731
    },
    {
        "station": "WQPC",
        "city": "Prairie du Chien",
                "channels": "94.3",
                "lat": 43.059706,
                "long": -91.100685
    },
    {
        "station": "WRDB",
        "city": "Reedsburg",
                "channels": "1400",
                "lat": 43.541647,
                "long": -90.034848
    },
    {
        "station": "WHOH",
        "city": "Rhinelander",
                "channels": "96.5",
                "lat": 45.667458,
                "long": -89.20818
    },
    {
        "station": "WJMC",
        "city": "Rice Lake",
                "channels": "1240",
                "lat": 45.508568,
                "long": -91.77406
    },
    {
        "station": "WRCO",
        "city": "Richland Center",
                "channels": "1450/107.7",
                "lat": 43.316095,
                "long": -90.377626
    },
    {
        "station": "WEVR",
        "city": "River Falls",
                "channels": "1550/106.3",
                "lat": 44.888578,
                "long": -92.650755
    },
    {
        "station": "WTCH",
        "city": "Shawano",
                "channels": "960/96.1",
                "lat": 44.780816,
                "long": -88.631214
    },
    {
        "station": "WHBL",
        "city": "Sheboygan",
                "channels": "1330",
                "lat": 43.720551,
                "long": -87.73453
    },
    {
        "station": "WSAU",
        "city": "Stevens Point",
                "channels": "99.9",
                "lat": 44.33858,
                "long": -89.648732
    },
    {
        "station": "WDOR",
        "city": "Sturgeon Bay",
                "channels": "93.9",
                "lat": 44.906384,
                "long": -87.370933
    },
    {
        "station": "WDUX",
        "city": "Waupaca",
                "channels": "92.7",
                "lat": 44.354286,
                "long": -89.058641
    },
    {
        "station": "WSAU",
        "city": "Wausau",
                "channels": "550/95.1",
                "lat": 44.857191,
                "long": -89.587065
    },
    {
        "station": "WJMS",
        "city": "Ironwood",
                "channels": "590",
                "lat": 46.424392,
                "long": -90.209342
    }
]

connection = pymongo.MongoClient(
    config.fields['dbHost'], config.fields['dbPort'])
db = connection[config.fields['dbName']]
db.authenticate(config.fields['dbUser'], config.fields['dbPass'])

# set a collection
stations = db['radio']
# creates and inserts collection and data
stations.insert_many(SEED_DATA)
# closes collection
connection.close()
