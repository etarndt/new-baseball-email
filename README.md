**Python script to send a text/email with with Brewer's game information.**

Data is gathered from:

- FoxSports.com (through scraping)
- mlbgame - an open source API for mlb game data
- pyicloud - iCloud API to retreive personal phone location

Data is sent using Mailgun Email API

**High-Level Process:**

1. If today's date doesn't match gameWrite.txt date (populated every time data is retreived), game info is retreived and text/email is sent
2. If game info has been retrieved but there is no game, nothing will happen
3. If game info has been retrieved and there is a game  
   3a. If within or equal to an hour before game time, send game info  
   3b. If outside of an hour of game time, nothing will happen

**Text/Email Format:**

Today's Date: D/M  
Matchup: Brewers (W/L) vs/at Opposing Team (W-L)  
Starters: MIL Pitcher (W-L) vs OPP Pitcher (W-L)  
Gametime: H:MMAM/PM  
Stadium: Home Stadium Name  
TV: TV Network(s)  
Radio Info -  
Closest Radio Station - 1  
Closest Radio Station - 2  
Closest Radio Station - 3

**Text/Email Sample:**

Today's Date: 4/19  
Matchup: Brewers (12-7) vs Los Angeles Dodgers (12-8)  
Starters: MIL J Chacín (2-2) vs LAD R Stripling (1-1)  
Gametime: 7:10PM  
Stadium: Miller Park  
TV: FSWI  
Radio Info -  
WTMJ - Milwaukee - 620/103.3  
WHBL - Sheboygan - 1330  
WFAW - Fort Atkinson - 940
