import sys
sys.path.append(".")
from resources import config
from pyicloud import PyiCloudService


def getiCloudPhoneLocationInfo():
    try:
        api = PyiCloudService(
            config.fields['iCloudEmail'], config.fields['iCloudKey'])
        locationInfo = None
        count = 0
        while locationInfo is None:
            if api.requires_2fa:
                import click
                print("Two-step authentication required. Your trusted devices are:")
                devices = api.trusted_devices
                for i, device in enumerate(devices):
                    print("  %s: %s" % (i, device.get('deviceName',
                                                      "SMS to %s" % device.get('phoneNumber'))))

                device = click.prompt(
                    'Which device would you like to use?', default=0)
                device = devices[device]
                if not api.send_verification_code(device):
                    print("Failed to send verification code")
                    raise Exception('Send Verification Code')

                code = click.prompt('Please enter validation code')
                if not api.validate_verification_code(device, code):
                    print("Failed to verify verification code")
                    raise Exception('Verify Verification Code')
            # get location info from phone using ID
            # print(api.devices) --> prints devices on iCloud account
            locationInfo = api.devices[config.fields['phoneID']].location()

            # test when None is returned
            # locationInfo = api.devices[config.fields['phoneID2']].location()

            # count the number of loops
            count = count + 1

            # if number of loops hits 100, return nothing
            if count > 99:
                return None

        # store latitude and longitude
        latitude = locationInfo['latitude']
        longitude = locationInfo['longitude']

        # list to store latitude and longitude
        latLong = [latitude, longitude]
        # print(str(latitude) + ' ' + str(longitude))

        print('Current iPhone coordinates: ' +
              str(latLong[0]) + ',' + str(latLong[1]))
        # return location list
        return latLong
    except Exception as e:
        print('getiCloudPhoneLocationInfo Failed at ' + str(e))
        return None


# print(getiCloudPhoneLocationInfo())
# getiCloudPhoneLocationInfo()
