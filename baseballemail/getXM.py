from selenium import webdriver
import sys
import re


def getXM():
    try:
        driver = webdriver.Chrome()
        driver.get('https://m.siriusxm.com/mlbschedule')
        table = driver.find_element_by_xpath(
            "//div[@class='ui-grid-schedule']")
        rows = table.find_elements_by_xpath(".//ul/li")
        print(len(rows))
        count = -1
        for x in rows:
            # print(x.text)
            count = count + 1
            # look for word in text
            pattern = re.compile('Tigers')
            find = pattern.findall(x.text)
            # if found break
            if len(find) > 0:
                break
            # if not found, check if it's last row check
            else:
                if count == len(rows) - 1:
                    # if info isn't found
                    return(['XM Info Not Found'])
    except Exception as e:
        print(e)
        return(['Error Retrieving XM Info'])

    return rows[count].text.split('\n')[2:]
