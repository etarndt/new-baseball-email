import mlbgame
import datetime
from pytz import timezone


def getTeamRecord(teamShort):

    record = 'NRF'  # no record found

    # get divisions
    try:
        divisions = mlbgame.info.standings(
            datetime.datetime.now(timezone('US/Central'))).get('divisions')

        for div in range(0, len(divisions)):  # iterate divisions
            for tm in range(0, len(divisions[div].get('teams'))):  # iterate teams
                team = divisions[div].get('teams')[tm]
                # print all teams as the iteration processes
                # print(team.get('team_full'))
                if (teamShort in team.get('team_full')):  # search team info
                    # print(team.get('w') + '-' + team.get('l')) # print record
                    record = '(' + team.get('w') + '-' + team.get('l') + ')'
        return record
    except:
        print('team record failure')
        return ''


# print(getTeamRecord('Brewers'))
