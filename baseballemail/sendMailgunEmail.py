import requests
import sys
sys.path.append(".")
from resources import config


def sendEmail(emailBody):

    response = requests.post(
        "https://api.mailgun.net/v3/" +
        config.fields['mailgunDomain'] + "/messages",
        auth=("api", config.fields['mailgunAPIKey']),
        data={"from": "Excited User <mailgun@" + config.fields['mailgunDomain'] + ">",
              "to": config.fields['text'],
              "subject": "",
              "text": emailBody})

    print(response)

    return
