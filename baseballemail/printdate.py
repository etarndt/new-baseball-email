import datetime
from pytz import timezone


def printdatetimefunction():
    month = datetime.datetime.now(timezone('US/Central')).strftime('%m')
    day = datetime.datetime.now(timezone('US/Central')).strftime('%d')
    if ((int(month) - 10) < 0):
        month = str(month)[1:]
    if ((int(day) - 10) < 0):
        day = str(day)[1:]
    return (month + '/' + day)
