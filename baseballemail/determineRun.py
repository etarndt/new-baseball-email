import sys
import ast
import os

# Determine what to do for this script run


def determineRun(dayMonth):
    date = dayMonth
    gameInfoRetrieved = False
    game = True

    # read in gameWrite.txt file - where data will be
    __location__ = os.path.realpath(os.path.join(
        os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(__location__, 'gameWrite.txt'), 'r') as contentFile:
        # conver string to list
        gameWriteList = ast.literal_eval(contentFile.read().split('\n')[0])
    # if dates match then game info has already been retrieved
    if (gameWriteList[0] == date):
        gameInfoRetrieved = True
    # if no game today exists, then there is no game for the date
    if (gameWriteList[1] == 'No game today!'):
        game = False

    # if game info has not been returned yet today, return 1 (meaning go get game info)
    if (gameInfoRetrieved == False):
        return 1
    # if game info has been retrieved but there is no game, return 2 (meaning do nothing)
    if (gameInfoRetrieved and game == False):
        return 2
    # if game info has been retrieved and there is a game, return 3 (send in run(s) within
    # an hour from gametime)
    if (gameInfoRetrieved and game):
        return 3

# First Run: Get game details, send an email regardless - either [game details + XM details] + local radio or no game
# Subsequent runs: Don't get details, and if there is a game,
# send an email only if within an hour of [gametime + XM details] + local radio details
# determineRun('8/6')
