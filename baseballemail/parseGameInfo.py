from .convertEasternToCentral import convertEasternToCentral
from .findTVFromBrewers import findTVFromBrewers
from .getTeamRecord import getTeamRecord
import sys


def parseGameInfo(list):
    #list = ['8/10', 'AT', 'ATLANTA\nBRAVES (59-47)', '7:35p ET\nFSWI, FSSO', 'MIL F Peralta (4-2)\nATL J Teherán (8-7)', 'SunTrust Park\nTickets Odds']
    #list = ['8/15', 'AT', 'ST. LOUIS\nCARDINALS (56-54)', '2:20p ET\nFSWI', '', 'Wrigley Field\nTickets Odds']
    # print(list)

    # parse team name and record
    tempList1 = list[2].split(' ')
    # opposing team name/record
    # if there is no space in the first name of the team
    # parse respectively and correct capitalization
    if len(tempList1) == 2:
        opposingTeamNameRecord = tempList1[0].split('\n')[0].capitalize() + ' ' + tempList1[0].split('\n')[1].capitalize() \
            + ' ' + tempList1[1]
    # if there is a space in the first name of the team
    # parse respectively and correct capitalization
    elif len(tempList1) == 3:
        opposingTeamNameRecord = tempList1[0].capitalize() + ' ' + tempList1[1].split('\n')[0].capitalize() + ' ' \
            + tempList1[1].split('\n')[1].capitalize() + ' ' + tempList1[2]

    # parse time and tv networks
    tempList2 = list[3].split('\n')
    # time
    gameTime = tempList2[0].split(' ')[0]
    # convert time into HH:MM AM/PM
    if gameTime.endswith('p'):
        gameTime = gameTime[:-1] + 'PM'
    else:
        gameTime = gameTime[:-1] + 'AM'
    # convert EST to CST
    # print(gameTime)
    gameTime = convertEasternToCentral(gameTime)
    # # tv networks
    tvNetworks = findTVFromBrewers(list[0])
    # # if temp list has length of 2, there is a tv network
    # if (len(tempList2) == 2):
    #     tvNetworks = tempList2[1]
    # # if tv network is not available pass TBD
    # else:
    #     tvNetworks = 'TBD'

    # parse starting pitchers
    # check if starting pitchers exist - if not, fill home and away starter w/ TBD
    if list[4] == '':
        awayStarter = 'TBD'
        homeStarter = 'TBD'
    # else parse and assign starters
    else:
        tempList3 = list[4].split('\n')
        # awayStarter
        awayStarter = tempList3[0]
        # homeStarter
        homeStarter = tempList3[1]

    # parse stadium
    tempList4 = list[5].split('\n')
    # stadium
    stadium = tempList4[0]

    returnList = [list[0], list[1].lower(), opposingTeamNameRecord, gameTime, tvNetworks, awayStarter,
                  homeStarter, stadium, getTeamRecord('Brewers')]

    print(returnList)
    return returnList
# parseGameInfo([])
