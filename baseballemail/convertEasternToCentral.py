from datetime import datetime
from pytz import timezone
import pytz
import sys
## function to conver est to cst
def convertEasternToCentral(stringESTGameTime):
    beforeHours = int(stringESTGameTime.split(':')[0])
    minutes = stringESTGameTime.split(':')[1][:-2]
    beforeAMPM = stringESTGameTime.split(':')[1][2:]

    #print(str(beforeHours) + ':' + minutes + beforeAMPM)
    
    if (beforeHours - 1 == 11):
        afterHours = 11
        if beforeAMPM == 'AM':
            afterAMPM = 'PM'
        else:
            afterAMPM = 'AM'
    else:
        afterHours = beforeHours - 1
        if afterHours == 0:
            afterHours = 12
        afterAMPM = beforeAMPM
        #print('test')

    #print(str(afterHours) + ':' + minutes + afterAMPM)
    return str(afterHours) + ':' + minutes + afterAMPM

#convertEasternToCentral('12:00PM')