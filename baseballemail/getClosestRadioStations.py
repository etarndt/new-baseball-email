import sys
sys.path.append(".")
import pymongo
from resources import config
import math
import operator


def getClosestRadioStations(lat, long):
    # db connection details
    connection = pymongo.MongoClient(
        config.fields['dbHost'], config.fields['dbPort'])
    db = connection[config.fields['dbName']]
    db.authenticate(config.fields['dbUser'], config.fields['dbPass'])

    # set a collection
    stationsCollection = db['radio']

    # valid lat
    if lat != -400:
        # finds all stations
        stations = stationsCollection.find()

        distances = {}
        count = 0
        strings = []
        for station in stations:
            # print(station['lat'])
            strings.append(station['station'] + ' - ' +
                           station['city'] + ' - ' + station['channels'])
            distances[count] = math.sqrt(
                (station['lat'] - lat)**2 + (station['long'] - long)**2)
            count = count + 1

        # sort distances
        sortedDistances = sorted(distances.items(), key=operator.itemgetter(1))

        # concat closest 3 stations
        closestStations = ''
        for x in range(0, 3):
            # print(strings[sortedDistances[x][0]])
            if (x == 0):
                closestStations = strings[sortedDistances[x][0]]
            else:
                closestStations = closestStations + \
                    '\n' + strings[sortedDistances[x][0]]

    # handle invalid case
    else:
        # finds all stations
        #stations = stationsCollection.find({"$or:" [{"station": "WTMJ"}, {"station": "WHBY"}, {"station": "WACD"}]})

        stations = stationsCollection.find(
            {"station": {"$in": ["WTMJ", "WIGM", "WIBA"]}})

        #stations = stationsCollection.find({"station": "WTMJ"})

        distances = {}
        count = 0
        strings = []
        for station in stations:
            # print(station['lat'])
            strings.append(station['station'] + ' - ' +
                           station['city'] + ' - ' + station['channels'])
            distances[count] = math.sqrt(
                (station['lat'] - lat)**2 + (station['long'] - long)**2)
            count = count + 1

        # sort distances
        sortedDistances = sorted(distances.items(), key=operator.itemgetter(1))

        # concat closest 3 stations
        closestStations = ''
        for x in range(0, 3):
            # print(strings[sortedDistances[x][0]])
            if (x == 0):
                closestStations = strings[sortedDistances[x][0]]
            else:
                closestStations = closestStations + \
                    '\n' + strings[sortedDistances[x][0]]

    # closes collection
    connection.close()

    return closestStations


# yes = getClosestRadioStations(43.05065215268321, -
#                              87.90512211408065)  # test milwaukee
# latLong = [-400, -400]
# lat = latLong[0]
# lon = latLong[1]
# yes = getClosestRadioStations(lat, lon)  # test milwaukee
# print(yes)
