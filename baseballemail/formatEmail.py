from .getXM import getXM
import sys
import ast
import os
from .getClosestRadioStations import getClosestRadioStations
from .getiCloudPhoneLocationInfo import getiCloudPhoneLocationInfo


def formatEmail():
    # read in gameWrite.txt file - where data will be
    __location__ = os.path.realpath(os.path.join(
        os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(__location__, 'gameWrite.txt'), 'r') as contentFile:
        # conver string to list
        gameWriteList = ast.literal_eval(contentFile.read().split('\n')[0])

    if (gameWriteList[1] == 'No game today!'):
        # email string with 'No Game' info
        emailString = gameWriteList[1]
    else:
        # formatting for brewers team record failure
        if gameWriteList[8] != '':
            gameWriteList[8] = ' ' + gameWriteList[8] + ' '
        else:
            gameWriteList[8] = ' '

        # email string with game info
        emailString = "Today's Date: " + gameWriteList[0] + '\n' + \
            "Matchup: Brewers" + gameWriteList[8] + gameWriteList[1] + " " + gameWriteList[2] + '\n' + \
            "Starters: " + gameWriteList[6] + " vs " + gameWriteList[5] + '\n' + \
            "Gametime: " + gameWriteList[3] + '\n' + \
            "Stadium: " + gameWriteList[7] + '\n' + \
            "TV: " + gameWriteList[4] + '\n'
        # add XM info
        # emailString = emailString + 'XM Info -\n'
        # for x in getXM():
        #     emailString = emailString + x + '\n'
        # TODO add local radio info
        emailString = emailString + 'Radio Info -\n'
        latLong = getiCloudPhoneLocationInfo()

        # if failure, pass -400,-400. Else pass correct values
        if latLong is None:
            emailString = emailString + getClosestRadioStations(-400, -400)
        else:
            emailString = emailString + \
                getClosestRadioStations(latLong[0], latLong[1])

    print(emailString)
    return emailString

# returnList = [list[0],list[1],opposingTeamNameRecord,gameTime,tvNetworks,awayStarter, \
#    homeStarter,stadium]
