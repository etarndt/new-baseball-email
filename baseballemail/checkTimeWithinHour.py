import sys
import ast
import datetime
import os
from pytz import timezone


def checkTimeWithinHour():
    __location__ = os.path.realpath(os.path.join(
        os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(__location__, 'gameWrite.txt'), 'r') as contentFile:
        gameWriteList = ast.literal_eval(contentFile.read().split('\n')[0])
        #gameWriteList[3] = '10:48PM'
        timeList = gameWriteList[3].split(':')

        # conversion to military time
        if timeList[1][2] == 'P' and timeList[0] != '12':
            timeList[0] = str(int(timeList[0])+12)
        if timeList[1][2] == 'A' and timeList[0] == '12':
            timeList[0] = '00'

        # parse time
        gameTime = timeList[0] + ':' + timeList[1][0] + timeList[1][1]

        # convert string to time
        dateTimeGameTime = datetime.datetime.strptime(gameTime, '%H:%M')
        # print('dateTimeGameTime: ' + dateTimeGameTime.strftime("%m/%d/%Y, %H:%M:%S"))

        currentTime = datetime.datetime.now(timezone('US/Central'))
        # print('currentTime: ' + currentTime.strftime("%m/%d/%Y, %H:%M:%S"))

        dateTimeGameTimeMinusHour = dateTimeGameTime - \
            datetime.timedelta(hours=1)
        # print('dateTimeGameTimeMinusHour: ' + dateTimeGameTimeMinusHour.strftime("%m/%d/%Y, %H:%M:%S"))

        # check if within an hour
        if (currentTime.time() >= dateTimeGameTimeMinusHour.time()) and (currentTime.time() < dateTimeGameTime.time()):
            return True
        else:
            return False


# print(checkTimeWithinHour())
