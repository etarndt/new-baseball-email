from .printdate import printdatetimefunction


def getGamesURL(date):
    monthDay = date.split('/')
    monthDay = [int(monthDay[0]), int(monthDay[1])]
    springTrainingURL = 'https://www.foxsports.com/mlb/milwaukee-brewers-team-schedule?season=2019'
    regularSeasonURL = 'https://www.foxsports.com/mlb/milwaukee-brewers-team-schedule?season=2019&seasonType=1&month=-1'

    if (monthDay[0] < 3):
        return springTrainingURL
    elif (monthDay[0] == 3):
        if (monthDay[1] >= 28):
            return regularSeasonURL
        else:
            return springTrainingURL
    else:
        return regularSeasonURL
