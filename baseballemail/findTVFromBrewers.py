from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from datetime import datetime
import os


def findTVFromBrewers(dayMonth):
    date = dayMonth
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(options=chrome_options)
    driver.get('https://www.mlb.com/brewers/schedule/2019/fullseason')
    x = driver.find_element_by_class_name('container').find_elements_by_class_name(
        'list-mode-table-wrapper')

    # convert M/D datetime to three letter word and date, eg Apr 18
    datetime_object = datetime.strptime(date, '%m/%d')
    shortMonthDay = datetime_object.strftime('%b %-d')
    # print(shortMonthDay)

    match = 0
    matchDayInfo = ""
    for info in x:
        dayInfo = info.find_element_by_class_name('month-date')
        if(dayInfo.text == shortMonthDay):
            match = 1
            matchDayInfo = info
            # print('yes')

    if(match == 1):
        tv = matchDayInfo.find_element_by_class_name(
            'broadcasters').find_element_by_class_name('tv').find_element_by_class_name('list').text

        return tv

    return ''


# print(findTVFromBrewers('4/15'))
