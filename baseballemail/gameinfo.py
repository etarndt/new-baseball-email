from selenium import webdriver
from .parseGameInfo import parseGameInfo
from .getGamesURL import getGamesURL
from selenium.webdriver.chrome.options import Options
import os


def findgameinfo(dayMonth):
    date = dayMonth
    chrome_options = Options()
    chrome_options.add_argument("--window-size=1920,1080")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--proxy-server='direct://'")
    chrome_options.add_argument("--proxy-bypass-list=*")
    chrome_options.add_argument("--start-maximized")
    chrome_options.add_argument('--headless')
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(options=chrome_options)
    gamesURL = getGamesURL(date)
    driver.get(gamesURL)
    #p_element = driver.find_element_by_class_name('g5-component--mlb-scores__date')
    table = driver.find_element_by_xpath(
        "//table[@class='wisbb_scheduleTable']")
    rows = table.find_elements_by_xpath(".//tr")
    count = 0
    match = 0
    for x in rows[1:]:
        count = count + 1
        col = x.find_elements_by_xpath(".//td")
        # print(col[0].text)
        if (date == col[0].text):
            match = 1
            break

    # insert data into list
    data = []
    for z in range(len(col)):
        data.append(col[z].text)
    print(data)
    if (match == 0):
        # Write 'No Date Match to text file
        # with open("gameWrite.txt", "w") as text_file:
            #print(f"{date}\n\n\n\n\n\n\n\n\n\n\n\n\nNo game today!", file=text_file, end="")
        data = [dayMonth, 'No game today!']
    else:
        # Write game information to text file
        # with open("gameWrite.txt", "w") as text_file:
            #print(f"{rows[count].text}", file=text_file)
            # parse game info
        data = parseGameInfo(data)
        # return(data)
    # print(data)
    __location__ = os.path.realpath(os.path.join(
        os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(__location__, 'gameWrite.txt'), 'w') as text_file:
        print(data, file=text_file)
    return data


# rows = table.find_elements_by_xpath(".//tr")
# for span in all_spans:
#     if (span.text != ""):
#         print(span.text)
# findgameinfo('4/15')
