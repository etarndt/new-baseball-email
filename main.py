from baseballemail.gameinfo import findgameinfo
from baseballemail.determineRun import determineRun
from baseballemail.printdate import printdatetimefunction
from baseballemail.formatEmail import formatEmail
from baseballemail.checkTimeWithinHour import checkTimeWithinHour
from baseballemail.sendMailgunEmail import sendEmail
from baseballemail.sendMailgunEmailAdmin import sendEmailAdmin
import datetime
from pytz import timezone
import sys
import traceback

try:
    # First Run: Get game details, send an email regardless
    #  - either [game details + XM details] + local radio or no game
    # Subsequent runs: Don't get details, and if there is a game,
    # send an email only if within an hour of [gametime + XM details]
    #  + local radio details
    print('Runtime CST: ' + datetime.datetime.now(timezone('US/Central')
                                                  ).strftime("%m/%d/%Y, %H:%M:%S"))
    dayMonth = printdatetimefunction()
    # dayMonth = '5/10'  # use to test a specific date and other scenarios
    runCode = determineRun(dayMonth)
    print('Run Code: ' + str(runCode))
    # sys.exit()

    # if today's date doesn't match file date, get game info and send email
    if (runCode == 1):
        findgameinfo(dayMonth)
        # function for XM radio info, add to file
        # function(s) for local radio info, add to file
        # with open('gameWrite.txt', 'r') as contentFile:
        # sendEmail(contentFile.read())
        sendEmail(formatEmail())
    # if today's date does match file date (info gathered) and no game, do nothing
    if (runCode == 2):
        # do nothing
        print('doing nothing')
        # sendEmail('2nd run code')  # testing purposes
    if (runCode == 3):
        # if time is within one hour of game
        if (checkTimeWithinHour()):
            # function(s) for local radio info, add to file
            # send email
            print('3rd runCode')
            sendEmail(formatEmail())
        # else:
            # do nothing
            # sendEmail('3rd run code & not within 1 hour')  # testing purposes
except Exception:
    sendEmailAdmin('Baseball Email Failure. Check logs.')
    print('Top-level Exception:\n' + traceback.format_exc())
